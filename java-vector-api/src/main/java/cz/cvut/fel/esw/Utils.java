package cz.cvut.fel.esw;

/**
 * @author Marek Cuchý (CVUT)
 */
public class Utils {

    public static int sum(int[] array) {
        int sum = 0;
        for (int i : array) {
            sum += i;
        }
        return sum;
    }

    public static int dot(int[] v1, int[] v2) {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException();
        }
        int sum = 0;
        for (int i = 0; i < v1.length; i++) {
            sum += v1[i] * v2[i];
        }
        return sum;
    }

    public static Matrix multiply(Matrix left, Matrix right) {
        int[][] a = left.rowBased();
        int[][] bt = right.columnBased();
        int n = left.rows();
        int m = left.columns();
        int p = right.columns();

        int[][] res = new int[n][p];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < p; j++) {
                for (int k = 0; k < m; k++) {
                    res[i][j] += a[i][k] * bt[j][k];
                }
            }
        }
        return new Matrix(res);
    }
}
